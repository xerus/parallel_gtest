# gtest runner
Enables to run google tests in parallel.

[![pipeline status](https://gitlab.com/radek.benes1/parallel_gtest/badges/master/pipeline.svg)](https://gitlab.com/radek.benes1/parallel_gtest/-/commits/master)

[![coverage report](https://gitlab.com/radek.benes1/parallel_gtest/badges/master/coverage.svg)](https://gitlab.com/radek.benes1/parallel_gtest/-/commits/master)

## usage

### Installation:
```
pip install git+https://gitlab.com/radek.benes1/parallel_gtest@master#egg=parallel-gtest
```

### Running

```
parallel-gtest path/to/binary
```
