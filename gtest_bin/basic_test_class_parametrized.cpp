#include "gtest/gtest.h"

class MultiplyTestP : public ::testing::TestWithParam<int> {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_P(MultiplyTestP, TwoValues) {
    const int val = GetParam();
    EXPECT_EQ(0, 0 * val);
}

INSTANTIATE_TEST_SUITE_P(P, MultiplyTestP, ::testing::Values(0, 1, 2));
