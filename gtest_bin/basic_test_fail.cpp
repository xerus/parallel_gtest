#include <gtest/gtest.h>

#include <string>
using std::string;

TEST(IntCompareFail, Exact) {
    EXPECT_EQ(6, 5);
}

TEST(IntCompareFail, GT) {
    EXPECT_GT(4, 5);
}

TEST(IntCompareFail, Near) {
    EXPECT_NEAR(5, 2, 2);
}
