import pytest

from parallel_gtest.common_tools import ProcessOutput
from parallel_gtest.gtest_exceptions import BadTestBinary
from parallel_gtest.gtest_runner import GTestRunner


def test_listing_bad_binary(bin_path):
    def failing_call():
        GTestRunner(bin_path + "xxx").list_tests()

    with pytest.raises(BadTestBinary, match="Bad binary"):
        failing_call()


def test_listing(bin_path):
    gtest_listings = GTestRunner(bin_path).list_tests()

    assert 3 == gtest_listings.test_suites_count
    assert 8 == gtest_listings.test_count

    assert gtest_listings.test_suite_names == [
        "IntCompare",
        "MultiplyTest",
        "P/MultiplyTestP",
    ]


def test_listing_mock(bin_path, mocker):
    mocker.patch(
        "parallel_gtest.gtest_runner.run_executable",
        return_value=ProcessOutput(
            0,
            [
                "IntCompare.",
                "  Exact",
                "  GT",
                "  Near",
                "MultiplyTest.",
                "  TwoValues",
                "  ThreeValues",
            ],
        ),
    )

    gtest_listings = GTestRunner(bin_path).list_tests()

    assert 2 == gtest_listings.test_suites_count
    assert 5 == gtest_listings.test_count

    assert gtest_listings.test_suite_names == ["IntCompare", "MultiplyTest"]


def test_listing_filter(bin_path):
    gtest_listings = GTestRunner(bin_path, filter="Int*").list_tests()
    assert 1 == gtest_listings.test_suites_count
    assert 3 == gtest_listings.test_count

    assert gtest_listings.test_suite_names == ["IntCompare"]


def test_listing_filter_neg(bin_path):
    gtest_listings = GTestRunner(
        bin_path, other_params="--gtest_filter=-Int*"
    ).list_tests()
    assert 2 == gtest_listings.test_suites_count
    assert 5 == gtest_listings.test_count

    assert gtest_listings.test_suite_names == ["MultiplyTest", "P/MultiplyTestP"]
