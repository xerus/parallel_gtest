import pytest

from parallel_gtest.gtest_tools import (
    _parametrized_test_name,
    _test_name,
    _test_suite_name,
    parse_listing_output,
)


@pytest.fixture(scope="module")
def gtest_list_output():
    return [
        "IntCompare.",
        "  Exact",
        "  GT",
        "  Near",
        "MultiplyTest.",
        "  TwoValues",
        "  ThreeValues",
    ]


@pytest.fixture(scope="module")
def gtest_list_output_parametrized():
    return [
        "some log",
        "another log",
        "log",
        "TestSuite.",
        '  ParamSetup/0  # GetParam() = "a"',
        '  ParamSetup/1  # GetParam() = "b"',
        '  ParamSetup/2  # GetParam() = "c"',
        '  ParamSetup/3  # GetParam() = "d"',
    ]


def test_parse_listing_output(gtest_list_output):
    parsed = parse_listing_output(gtest_list_output)

    assert parsed.test_suite_names == ["IntCompare", "MultiplyTest"]
    assert parsed.test_names == [
        "IntCompare.Exact",
        "IntCompare.GT",
        "IntCompare.Near",
        "MultiplyTest.TwoValues",
        "MultiplyTest.ThreeValues",
    ]


def test_parse_listing_output_parametrized(gtest_list_output_parametrized):
    parsed = parse_listing_output(gtest_list_output_parametrized)

    assert parsed.test_suite_names == ["TestSuite"]
    assert parsed.test_names == [
        "TestSuite.ParamSetup/0",
        "TestSuite.ParamSetup/1",
        "TestSuite.ParamSetup/2",
        "TestSuite.ParamSetup/3",
    ]


def test_test_suite_name_valid():
    assert _test_suite_name("IntCompare.") == "IntCompare"


@pytest.mark.parametrize(
    "line",
    [
        "  IntCompare",
        "some dummy comment",
        "comment",
        '  ParametrizedTest/0  # GetParam() = "a"',
    ],
)
def test_test_suite_name_invalid(line):
    with pytest.raises(ValueError, match="Not test suite"):
        _test_suite_name(line)


def test_test_name_valid():
    assert _test_name("  testa") == "testa"


@pytest.mark.parametrize(
    "line",
    [
        "IntCompare",
        "IntCompare.",
        "some dummy comment",
        "comment",
        '  ParametrizedTest/0  # GetParam() = "a"',
    ],
)
def test_test_name_invalid(line):
    with pytest.raises(ValueError, match="Not test"):
        _test_name(line)


def test_parametrized_test_name_valid():
    assert (
        _parametrized_test_name('  ParametrizedTest/0  # GetParam() = "a"')
        == "ParametrizedTest/0"
    )


@pytest.mark.parametrize(
    "line",
    [
        "IntCompare",
        "IntCompare.",
        "some dummy comment",
        "comment",
        'ParametrizedTest/0  # GetParam() = "a"',
    ],
)
def test_parametrized_test_name_invalid(line):
    with pytest.raises(ValueError, match="Not parametrized test"):
        _parametrized_test_name(line)
