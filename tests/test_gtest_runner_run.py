import pytest

from parallel_gtest.gtest_exceptions import BadTestBinary
from parallel_gtest.gtest_runner import GTestRunner

# from bin_fixtures import bin_path, bin_path_fail


def test_run_test(bin_path):
    gtest_runner = GTestRunner(bin_path)
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_names[0]

    test_result = gtest_runner.run_test(test_name)

    assert test_name == test_result.test_name
    assert 11 == len(test_result.output)


def test_run_test_bad_binary(bin_path):
    with pytest.raises(BadTestBinary, match="Bad binary"):
        gtest_runner = GTestRunner(bin_path + "_non_existing")
        gtest_runner.run_test("a")


def test_run_test_fail(bin_path_fail):
    gtest_runner = GTestRunner(bin_path_fail)
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_names[0]

    result = gtest_runner.run_test(test_name)
    assert not result.is_success


def test_run_tests(bin_path):
    gtest_runner = GTestRunner(bin_path)
    gtest_listing = gtest_runner.list_tests()

    test_results = gtest_runner.run_tests(gtest_listing.test_names)

    assert len(test_results) == gtest_listing.test_count
    test_names_from_res = [t.test_name for t in test_results]
    assert gtest_listing.test_names == test_names_from_res


def test_run_tests_fail(bin_path_fail):
    gtest_runner = GTestRunner(bin_path_fail)
    gtest_listing = gtest_runner.list_tests()

    results = gtest_runner.run_tests(gtest_listing.test_names, 3)

    fails = [not result.is_success for result in results]
    assert any(fails)


def test_run_test_repeat(bin_path):
    repeat_cnt = 3
    gtest_runner = GTestRunner(bin_path, other_params=f"--gtest_repeat={repeat_cnt}")
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_names[0]

    test_result = gtest_runner.run_test(test_name)

    assert test_name == test_result.test_name
    assert repeat_cnt * 11 + repeat_cnt * 3 == len(test_result.output)
