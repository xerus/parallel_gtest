from parallel_gtest.common_tools import format_output


def test_format_output():
    to_be_formated = ["my\n", "output\n"]

    expected = "my\noutput"
    formated = format_output(to_be_formated)
    assert expected == formated
