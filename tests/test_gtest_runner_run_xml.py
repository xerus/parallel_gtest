import os
import tempfile

from parallel_gtest.gtest_runner import GTestRunner


def test_run_test_check_xml(bin_path):
    gtest_runner = GTestRunner(bin_path)
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_names[0]

    test_result = gtest_runner.run_test(test_name)

    xml = test_result.xml_out

    suite_names = []
    test_names = []
    for suite in xml:
        suite_names.append(suite.name)
        for case in suite:
            test_names.append(case.name)

    assert suite_names == ["IntCompare"]
    assert test_names == ["Exact"]


def test_run_test_suite_check_xml(bin_path):
    gtest_runner = GTestRunner(bin_path)
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_suite_names[0]

    test_result = gtest_runner.run_test(test_name)

    xml = test_result.xml_out

    suite_names = []
    test_names = []
    for suite in xml:
        suite_names.append(suite.name)
        for case in suite:
            test_names.append(case.name)

    assert suite_names == ["IntCompare"]
    assert test_names == ["Exact", "GT", "Near"]


def test_run_test_suites_check_xml(bin_path):
    temp_dir = tempfile.gettempdir()
    temp_xml = os.path.join(temp_dir, "test_xml.xml")

    gtest_runner = GTestRunner(bin_path, xml_out=temp_xml)
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_suite_names[0]

    gtest_runner.run_tests([test_name])

    os.path.exists(temp_xml)

    os.remove(temp_xml)
