from typing import Iterable, Optional

from .common_tools import format_output


class BadTestBinary(Exception):
    def __init__(
        self,
        message: str,
        binary_name: str = "",
        failed_test_console: Optional[Iterable[str]] = None,
    ) -> None:
        super().__init__(message)
        self.binary_name = binary_name
        self.failed_test_console = failed_test_console

    def __str__(self) -> str:
        ret = f"Bad binary {self.binary_name}"
        ret += format_output(self.failed_test_console)
        return ret
