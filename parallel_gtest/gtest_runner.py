import multiprocessing
import os
import tempfile
import time
from typing import List, Optional

import attr
import tqdm
from junitparser import JUnitXml

from .common_tools import run_executable
from .gtest_executable import GTestExecutable
from .gtest_structure import GTestList
from .gtest_tools import parse_listing_output


@attr.s
class TestResult(object):
    test_name = attr.ib()
    is_success = attr.ib()
    output = attr.ib()
    duration = attr.ib()
    xml_out = attr.ib()


class GTestRunner(GTestExecutable):
    def __init__(
        self,
        binary,
        other_params: Optional[str] = None,
        filter: Optional[str] = None,
        xml_out: Optional[str] = None,
    ):
        """
        other_params: Optional[str]
            Other params to be passed to google test binary
        fileter: Optional[str]
            Filter to be passed to google test binary
        xml_out: Optional[str]
            Output file
        """
        super().__init__(binary)

        self._other_params = "" if other_params is None else other_params
        self._filter = "" if filter is None else f"--gtest_filter={filter}"
        self._xml_out = xml_out

    def list_tests(self) -> GTestList:
        """
        Lists all google tests
        """

        process_output = run_executable(
            f"{self.binary_name} --gtest_list_tests {self._other_params} {self._filter}",
            wd=self.working_directory,
            shell=True,
        )

        return parse_listing_output(process_output.stdout_lines)

    def _tmp_xml_path(self, test_name: str):
        test_name = test_name.replace("/", "_")

        temp_dir = tempfile.gettempdir()
        return os.path.join(temp_dir, f"tmp_{self._xml_out}_part_{test_name}")

    def run_test(self, test_name: str):
        start = time.time()
        command_parts = [
            self.binary_name,
            f"--gtest_filter={test_name}*",
            self._filter,
            self._other_params,
            f"--gtest_output=xml:{self._tmp_xml_path(test_name)}",
        ]

        command = " ".join(command_parts)

        process_output = run_executable(command, wd=self.working_directory, shell=True)

        duration = time.time() - start

        is_success = process_output.error_code == 0

        xml = JUnitXml.fromfile(os.path.join(self._tmp_xml_path(test_name)))
        os.remove(self._tmp_xml_path(test_name))

        return TestResult(
            test_name, is_success, process_output.stdout_lines, duration, xml
        )

    def run_tests(self, test_names: List[str], process_cnt: Optional[int] = 1):
        p = multiprocessing.Pool(process_cnt)

        results = []
        for result in tqdm.tqdm(
            p.imap_unordered(self.run_test, test_names), total=len(test_names)
        ):
            results.append(result)

        p.close()
        p.join()

        results = sorted(results, key=lambda x: test_names.index(x.test_name))

        if self._xml_out:
            xml = JUnitXml()
            for res in results:
                xml += res.xml_out
            xml.write(self._xml_out)

        return results
