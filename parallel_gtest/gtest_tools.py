import re
from typing import Iterable

from .gtest_structure import GTestList, GTestTestSuite


def _test_suite_name(line):
    search_obj = re.search(r"^([\S]*)\.$", line)

    if search_obj is None:
        raise ValueError("Not test suite")

    return search_obj.group(1)


def _test_name(line):
    search_obj = re.search(r"^  ([\S]*)$", line)

    if search_obj is None:
        raise ValueError("Not test")

    return search_obj.group(1)


def _parametrized_test_name(line):
    search_obj = re.search(r"^  ([\S]*\/\d*)  \# GetParam\(\) \= .*", line)

    if search_obj is None:
        raise ValueError("Not parametrized test")

    return search_obj.group(1)


def _is_valid_line(line):
    try:
        _test_suite_name(line)
        return True
    except ValueError:
        pass

    try:
        _test_name(line)
        return True
    except ValueError:
        pass

    try:
        _parametrized_test_name(line)
        return True
    except ValueError:
        return False


def parse_listing_output(raw_output: Iterable[str]) -> GTestList:
    test_list = GTestList()

    for list_output_line in raw_output:
        if not _is_valid_line(list_output_line):
            continue

        try:
            test_suite_name = _test_suite_name(list_output_line)
            test_list.add_tes_suite(GTestTestSuite(test_suite_name))

        except ValueError:
            try:
                test_name = _test_name(list_output_line)
            except ValueError:
                test_name = _parametrized_test_name(list_output_line)

            test_list.last_test_suite_added().add_test(test_name)

    return test_list
