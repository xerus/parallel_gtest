import subprocess
import sys
from typing import Iterable, Optional

import attr


def is_windows() -> bool:
    return sys.platform.startswith("win")


@attr.s
class ProcessOutput(object):
    error_code = attr.ib()
    stdout_lines = attr.ib()


def format_output(output_lines: Optional[Iterable[str]]) -> str:
    if output_lines is None:
        return ""

    output_lines = [out.replace("\n", "") for out in output_lines]
    return "\n".join(output_lines)


def run_executable(command: str, wd: str, shell: bool) -> ProcessOutput:
    p = subprocess.Popen(
        command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, cwd=wd, shell=shell
    )

    stdout = p.stdout
    stdout_lines = []

    if stdout is not None:
        for line in stdout.readlines():
            stdout_lines.append(str(line.decode()))

    exit_code = p.wait()
    return ProcessOutput(exit_code, stdout_lines)
