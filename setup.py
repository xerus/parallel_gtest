from distutils.core import setup


def get_long_description():
    with open("README.md", "r", encoding="utf-8") as fh:
        return fh.read()


setup(
    name="parallel_gtest",
    version="0.11",
    packages=["parallel_gtest", "parallel_gtest.tools"],
    license="MIT",
    description="Enables to run google tests in parallel.",
    long_description=get_long_description(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/radek.benes1/parallel_gtest",
    install_requires=["tqdm", "junitparser", "attrs"],
    extras_require={
        "dev": [
            "black==20.8b1",
            "pytest",
            "pytest-cov",
            "isort",
            "pre-commit",
            "flake8",
            "mypy",
            "cmake",
            "pytest-mock",
        ]
    },
    entry_points={
        "console_scripts": [
            "parallel-gtest = parallel_gtest.tools.parallel_gtest_run:main"
        ]
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Testing",
    ],
    python_requires=">=3.6",
)
